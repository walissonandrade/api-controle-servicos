﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Model;
using Negocio;

namespace ApiLavaTudo.Controllers
{
    public class OsController : ApiController
    {
        // GET api/Os
        public IEnumerable<Cliente> GetOss()
        {
            var objetos = new OrdemServicosNegocio().BuscarTudoLigadoOrdemServico();

            return objetos;
        }


        // GET api/Os/5
        public Cliente GetOs(long id)
        {
            OrdemServicosNegocio negocio = new OrdemServicosNegocio();

            OrdemServicos ordemServico = negocio.BuscarPorID(id);

            if (ordemServico == null)
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));

            return ordemServico.Cliente;
        }
    }
}