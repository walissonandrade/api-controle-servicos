﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Model;
using Negocio;

namespace ApiLavaTudo.Controllers
{
    public class ClienteController : ApiController
    {
        // GET api/Cliente
        public IEnumerable<List<OrdemServicos>> GetClientes()
        {
            var objetos = new ClienteNegocio().BuscarTodasOrdemServicosCliente();

            return objetos;
        }

        //Lista de OS realizadas pelo cliente
        // GET api/Cliente/5
        public List<OrdemServicos> GetCliente(long id)
        {
            ClienteNegocio negocio = new ClienteNegocio();

            Cliente cliente = negocio.BuscarPorID(id);

            if (cliente == null)
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));

            return cliente.OrdemServicos;
        }









        // PUT api/Cliente/5
        //public HttpResponseMessage PutCliente(int id, Cliente cliente)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        //    }

        //    //if (id != cliente.id)
        //    //{
        //    //    return Request.CreateResponse(HttpStatusCode.BadRequest);
        //    //}

        //    db.Entry(cliente).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException ex)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK);
        //}

        //// POST api/Cliente
        //public HttpResponseMessage PostCliente(Cliente cliente)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            db.Clientes.Add(cliente);
        //            db.SaveChanges();

        //            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, cliente);
        //            //response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = cliente.id }));
        //            return response;
        //        }
        //        else
        //        {
        //            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //// DELETE api/Cliente/5
        //public HttpResponseMessage DeleteCliente(int id)
        //{
        //    Cliente cliente = db.Clientes.Find(id);
        //    if (cliente == null)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.NotFound);
        //    }

        //    db.Clientes.Remove(cliente);

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException ex)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, cliente);
        //}
    }
}
