﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Negocio.Base;
using Model;
using Gestao.Vendas.Dispenser;
using Repositorio.Interface;

namespace Negocio
{
    public class OrdemServicosNegocio : BaseNegocio<OrdemServicos>
    {
        public OrdemServicosNegocio()
            : base(DispenserAplicacao.AcessoRepositorios<IOrdemServicosRepositorio>())
        {

        }

        // Função responsavel por buscar tudo que relaciona a OrdemServico
        public IEnumerable<Cliente> BuscarTudoLigadoOrdemServico()
        {

            return BuscarTodos().Select(s => s.Cliente);
        }

    }
}
