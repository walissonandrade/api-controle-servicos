﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Negocio.Base;
using Model;
using Gestao.Vendas.Dispenser;
using Repositorio.Interface;

namespace Negocio
{
    public class ClienteNegocio : BaseNegocio<Cliente>
    {
        public ClienteNegocio()
            : base(DispenserAplicacao.AcessoRepositorios<IClienteRepositorio>())
        {
        }

        // Função que busca todas as ordens de serviços relacionada com cliente
        public IEnumerable<List<OrdemServicos>> BuscarTodasOrdemServicosCliente()
        {
            return BuscarTodos().Select(s => s.OrdemServicos);
        }
    }
}
