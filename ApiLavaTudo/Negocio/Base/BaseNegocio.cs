﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repositorio.Base;
using System.Linq.Expressions;

namespace Negocio.Base
{
    // classe generica de regra de negocio
    public abstract class BaseNegocio<T> where T : BaseModel
    {
        protected IBaseRepositorio<T> Repositorio { get; set; }

        public BaseNegocio(IBaseRepositorio<T> aRepositorio)
        {
            Repositorio = aRepositorio;
        }

        public IEnumerable<T> BuscarTodos()
        {
            return Repositorio.BuscarTodos().ToList();
        }

        public IQueryable<T> BuscarPorFiltro(Expression<Func<T, bool>> aFiltro)
        {
            return Repositorio.BuscarPorFiltro(aFiltro);
        }

        public T BuscarPorID(long aIdentificador)
        {
            return Repositorio.BuscarPorID(aIdentificador);
        }

        public T Inserir(T aEntidade)
        {
            if (aEntidade == null)
                throw new Exception("A entidade que está tentando inserir está nula.");

            return Repositorio.Inserir(aEntidade);
        }

        public T Deletar(T aEntidade)
        {
            if (aEntidade == null)
                throw new Exception("A entidade que está tentando apagar está nula.");

            Repositorio.Deletar(aEntidade);
            return aEntidade;
        }

        public void Alterar(T aEntidade)
        {
            if (aEntidade == null)
                throw new Exception("A entidade que está tentando alterar está nula.");

            Repositorio.Alterar(aEntidade);
        }

        public void Salvar()
        {
            Repositorio.Salvar();
        }
    }
}