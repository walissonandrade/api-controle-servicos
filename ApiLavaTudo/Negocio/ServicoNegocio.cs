﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Negocio.Base;
using Model;
using Gestao.Vendas.Dispenser;
using Repositorio.Interface;

namespace Negocio
{
    public class ServicoNegocio : BaseNegocio<Servico>
    {
        public ServicoNegocio()
            : base(DispenserAplicacao.AcessoRepositorios<IServicoRepositorio>())
        {
        }

    }
}
