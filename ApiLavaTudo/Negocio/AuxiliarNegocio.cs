﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Negocio.Base;
using Model;
using Gestao.Vendas.Dispenser;
using Repositorio.Interface;

namespace Negocio
{
    public class AuxiliarNegocio : BaseNegocio<Auxiliar>
    {
        public AuxiliarNegocio()
            : base(DispenserAplicacao.AcessoRepositorios<IAuxiliarRepositorio>())
        { 
        }
    }
}
