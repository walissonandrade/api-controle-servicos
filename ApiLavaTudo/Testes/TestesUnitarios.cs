﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Gestao.Vendas.Dispenser;
using Model;
using Negocio;
using System.Configuration;

namespace Testes
{
    [TestClass]
    public class TestesUnitarios
    {
        [TestInitialize]
        public void Inicializar()
        {
            DispenserAplicacao.Initialize();
        }

        [TestMethod]
        public void InserirRegistros()
        {
            var x = string.Format(@"{0}", ConfigurationManager.AppSettings["ConexaoDB"]);

            Cliente cliente = new Cliente();
            ClienteNegocio clienteNegocio = new ClienteNegocio();
            
            cliente.Nome = "Matheus Rezende";
            cliente.TelefoneCelular = "98547-6589";
            cliente.TelefoneResidencial = "3322-1111";
            cliente.DataNascimento = new DateTime(1990, 01, 10);
            cliente.Email = "matheusrezende@gmail.com";

            clienteNegocio.Inserir(cliente);
            clienteNegocio.Salvar();
        }

        [TestMethod]
        public void BuscarOrdemServico()
        {
            var ordens = new ClienteNegocio().BuscarTodos().ToList();
        }

        [TestMethod]
        public void BuscarAuxiliar()
        {
            var auxiliar = new AuxiliarNegocio().BuscarTodos();
        }
    }
}
