﻿using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;
using Repositorio;
using Repositorio.Interface;
using System.Web.Mvc;

namespace Gestao.Vendas.Dispenser
{
    /// <summary>
    /// Classe responsável por carregar e instanciar as Dependências do projeto.
    /// </summary>
    public static class DispenserAplicacao
    {
        /// <summary>
        /// Inicializa todas as dependencias do projeto.
        /// </summary>
        public static void Initialize()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            container.RegisterType<IClienteRepositorio, ClienteRepositorio>();

            container.RegisterType<IServicoRepositorio, ServicoRepositorio>();

            container.RegisterType<IOrdemServicosRepositorio, OrdemServicosRepositorio>();

            container.RegisterType<IAuxiliarRepositorio, AuxiliarRepositorio>();

            return container;
        }

        /// <summary>
        /// Método responsável por acessar o repositorios de dados da aplicação.
        /// </summary>
        /// <typeparam name="T">Interfaces do Repositorio</typeparam>
        /// <returns></returns>
        public static T AcessoRepositorios<T>()
        {
            return DependencyResolver.Current.GetService<T>();
        }
    }
}
