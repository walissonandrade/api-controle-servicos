﻿using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio.Base
{
    // classe abstract com as funções de banco
    public abstract class BaseRepositorio<T> : IBaseRepositorio<T> where T : BaseModel
    {
        protected DbContext Contexto;

        protected IDbSet<T> _DbSet;

        public BaseRepositorio(DbContext aContexto)
        {
            Contexto = aContexto;
            _DbSet = aContexto.Set<T>();
        }

        public IQueryable<T> BuscarTodos()
        {
            return _DbSet.AsQueryable<T>();
        }

        public IQueryable<T> BuscarPorFiltro(Expression<Func<T, bool>> aFiltro)
        {
            return _DbSet.Where(aFiltro);
        }

        /// <summary>
        /// Busca uma entidade a partir da sua chave primária
        /// </summary>
        /// <param name="aIdentificador">Chave primária da entidade</param>
        /// <returns>Entidade do tipo T</returns>
        public T BuscarPorID(long aIdentificador)
        {
            return _DbSet.Find(aIdentificador);
        }

        public T Inserir(T aEntidade)
        {
            return _DbSet.Add(aEntidade);
        }

        public T Deletar(T aEntidade)
        {
            Contexto.Entry(aEntidade).State = EntityState.Deleted;
            return aEntidade;
        }

        public void Alterar(T aEntidade)
        {
            Contexto.Entry(aEntidade).State = EntityState.Modified;
        }

        public void Salvar()
        {
            Contexto.SaveChanges();
        }

        public void Dispose()
        {
            if (Contexto != null)
                Contexto.Dispose();

            GC.SuppressFinalize(this);
        }
    }
}