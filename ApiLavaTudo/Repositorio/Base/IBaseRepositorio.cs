﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio.Base
{   // interface base para a base repositorio
    public interface IBaseRepositorio<T> : IDisposable where T : BaseModel
    {
        IQueryable<T> BuscarTodos();
        IQueryable<T> BuscarPorFiltro(Expression<Func<T, bool>> aFiltro);
        T BuscarPorID(long aIdentificador);
        T Inserir(T aEntidade);
        T Deletar(T Entidade);
        void Alterar(T aEntidade);
        void Salvar();
    }
}
