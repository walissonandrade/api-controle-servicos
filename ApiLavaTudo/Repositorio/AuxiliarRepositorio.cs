﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repositorio.Base;
using Model;
using Repositorio.Interface;

namespace Repositorio
{
    public class AuxiliarRepositorio : BaseRepositorio<Auxiliar>, IAuxiliarRepositorio
    {
        public AuxiliarRepositorio(MeuContext aContexto)
            : base(aContexto)
        {
        }
    }
}
