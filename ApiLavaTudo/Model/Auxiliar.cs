﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
    [Table("Auxiliar")]
    public class Auxiliar : BaseModel
    {
        // chave estrangeira Servico
        public virtual Servico Servico { get; set; }
        
        // chave estrangeira OrdemServico
        public virtual OrdemServicos OrdemServicos { get; set; }
    }
}