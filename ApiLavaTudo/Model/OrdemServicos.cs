﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
    [Table("OrdemServicos")]
    public class OrdemServicos : BaseModel
    {
        public OrdemServicos()
        {
            Auxiliar = new List<Auxiliar>();
        }

        public DateTime DataContratacao { get; set; }

        // Chave estrangeira
        public virtual Cliente Cliente { get; set; }
        // Chave estrangeira
        public virtual List<Auxiliar> Auxiliar { get; set; }
    }
}