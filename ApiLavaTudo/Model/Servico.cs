﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
    [Table("Servico")]
    public class Servico : BaseModel
    {
        [Required]
        public string NomeServico { get; set; }
        public decimal ValorFinal { get; set; }
        public decimal CustoEmpresa { get; set; }
        public DateTime DataExecucao { get; set; }
        public string LogradouroExecucao { get; set; }
        public int NumeroExecucao { get; set; }
        public string Cidade { get; set; }
        public string EstadoExecucao { get; set; }
        public string PaisExecucao { get; set; }
    }
}