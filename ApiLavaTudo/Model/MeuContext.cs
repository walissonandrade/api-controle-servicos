﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Configuration;

namespace Model
{
    public class MeuContext : DbContext
    {
        public MeuContext()
            : base(string.Format(@"{0}", ConfigurationManager.AppSettings["ConexaoDB"]))
        {
        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Servico> Servicos { get; set; }
        public DbSet<Auxiliar> Auxiliares { get; set; }
        public DbSet<OrdemServicos> OrdemServicos { get; set; }
    }
}