﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
    [Table("Cliente")]
    public class Cliente : BaseModel
    {
        public Cliente()
        {
            OrdemServicos = new List<OrdemServicos>();
        }

        [Required]
        [MaxLength(80)]
        public string Nome { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public DateTime DataNascimento { get; set; }
        public string TelefoneCelular { get; set; }
        public string TelefoneResidencial { get; set; }

        // Chave estrangeira
        public virtual List<OrdemServicos> OrdemServicos { get; set; }
    }
}