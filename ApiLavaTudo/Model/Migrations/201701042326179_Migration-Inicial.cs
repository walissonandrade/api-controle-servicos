namespace Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrationInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Auxiliar",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        OrdemServicos_ID = c.Long(),
                        Servico_ID = c.Long(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.OrdemServicos", t => t.OrdemServicos_ID)
                .ForeignKey("dbo.Servico", t => t.Servico_ID)
                .Index(t => t.OrdemServicos_ID)
                .Index(t => t.Servico_ID);
            
            CreateTable(
                "dbo.OrdemServicos",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        DataContratacao = c.DateTime(nullable: false),
                        Cliente_ID = c.Long(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Cliente", t => t.Cliente_ID)
                .Index(t => t.Cliente_ID);
            
            CreateTable(
                "dbo.Cliente",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 80),
                        Email = c.String(nullable: false),
                        DataNascimento = c.DateTime(nullable: false),
                        TelefoneCelular = c.String(),
                        TelefoneResidencial = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Servico",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        NomeServico = c.String(nullable: false),
                        ValorFinal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CustoEmpresa = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DataExecucao = c.DateTime(nullable: false),
                        LogradouroExecucao = c.String(),
                        NumeroExecucao = c.Int(nullable: false),
                        Cidade = c.String(),
                        EstadoExecucao = c.String(),
                        PaisExecucao = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Auxiliar", "Servico_ID", "dbo.Servico");
            DropForeignKey("dbo.OrdemServicos", "Cliente_ID", "dbo.Cliente");
            DropForeignKey("dbo.Auxiliar", "OrdemServicos_ID", "dbo.OrdemServicos");
            DropIndex("dbo.OrdemServicos", new[] { "Cliente_ID" });
            DropIndex("dbo.Auxiliar", new[] { "Servico_ID" });
            DropIndex("dbo.Auxiliar", new[] { "OrdemServicos_ID" });
            DropTable("dbo.Servico");
            DropTable("dbo.Cliente");
            DropTable("dbo.OrdemServicos");
            DropTable("dbo.Auxiliar");
        }
    }
}
